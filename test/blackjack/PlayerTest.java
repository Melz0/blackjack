package blackjack;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {
    @Test
    void createPlayer_validName_shouldHaveCorrectName() {
        //arrange
        String expected = "Sam";
        Player sam = new Player("Sam");
        //act
        String actual = sam.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void setCards_validListOfCards_shouldHaveCorrectListOfCards() {
        //arrange
        Player sam = new Player("sam");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("HA");
        expected.add("D3");
        expected.add("CJ");
        expected.add("S2");
        //act
        sam.setCards(expected);
        ArrayList<String> actual = sam.getCards();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void setCards_validListOfCards_shouldHaveCorrectTotal() {
        //arrange
        int expected = 26;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("D3");
        cards.add("CJ");
        cards.add("S2");
        //act
        sam.setCards(cards);
        int actual = sam.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void addCard_validCard_shouldHaveCorrectListOfCards() {
        //arrange
        Player sam = new Player("sam");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("HA");
        expected.add("D3");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        sam.setCards(cards);
        //act
        sam.addCard("D3");
        ArrayList<String> actual = sam.getCards();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void addCard_validCard_shouldHaveCorrectTotal() {
        //arrange
        int expected = 14;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        sam.setCards(cards);
        //act
        sam.addCard("D3");
        int actual = sam.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void blackjack_validListOfCards_shouldReturnTrue() {
        //ararnge
        boolean expected = true;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("DQ");
        sam.setCards(cards);
        //act
        boolean actual = sam.blackJack();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void blackjack_invalidListOfCards_shouldReturnFalse() {
        //ararnge
        boolean expected = false;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("D2");
        sam.setCards(cards);
        //act
        boolean actual = sam.blackJack();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void has22_validListOfCards_shouldReturnTrue() {
        //arrange
        boolean expected = true;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("DA");
        sam.setCards(cards);
        //act
        boolean actual = sam.has22();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void has22_invalidListOfCards_shouldReturnFalse() {
        //arrange
        boolean expected = false;
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("D2");
        sam.setCards(cards);
        //act
        boolean actual = sam.has22();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void showCards_validListOfCards_shouldReturnStringWithCorrectNameAndCards() {
        //arrange
        String expected = "sam: HA, D2";
        Player sam = new Player("sam");
        ArrayList<String> cards = new ArrayList<>();
        cards.add("HA");
        cards.add("D2");
        sam.setCards(cards);
        //act
        StringBuilder actual = sam.showCards();
        //assert
        assertEquals(expected, actual.toString());
    }


}