package blackjack;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BlackjackTest {
    @Test
    void handOutCards_validPlayersAndListOfCards_shouldHaveValidListsOfSamsCards() {
        //arrange
        ArrayList<String> expected = new ArrayList<>();
        expected.add("HA");
        expected.add("S3");
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingHandOutCards();
        Blackjack blackjack = new Blackjack();
        //act
        blackjack.handOutCards(sam, dealer, deckOfCards);
        ArrayList<String> actual = sam.getCards();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void handOutCards_validPlayersAndListOfCards_shouldHaveValidListsOfDealersCards() {
        //arrange
        ArrayList<String> expected = new ArrayList<>();
        expected.add("D2");
        expected.add("S4");
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingHandOutCards();
        Blackjack blackjack = new Blackjack();
        //act
        blackjack.handOutCards(sam, dealer, deckOfCards);
        ArrayList<String> actual = dealer.getCards();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldHaveCorrectTotalForSamWhenSamWins() {
        //arrange
        int expected = 18;
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingSamAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(sam, dealer, deckOfCards);
        blackjack.addCardsToDeckForTestingSamAsWinner();
        //act
        blackjack.drawCards(sam, dealer, 4);
        int actual = sam.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldHaveCorrectTotalForDealerWhenSamWins() {
        //arrange
        int expected = 29;
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingSamAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(sam, dealer, deckOfCards);
        blackjack.addCardsToDeckForTestingSamAsWinner();
        //act
        blackjack.drawCards(sam, dealer, 4);
        int actual = dealer.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldReturnSam() {
        //arrange
        Player expected = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingSamAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(expected, dealer, deckOfCards);
        blackjack.addCardsToDeckForTestingSamAsWinner();
        //act
        Player actual = blackjack.drawCards(expected, dealer, 4);
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldHaveCorrectTotalForSamWhenDealerWins() {
        //arrange
        int expected = 17;
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingDealerAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(sam, dealer, deckOfCards);
        blackjack.addCardsToDeckForTestingDealerAsWinner();
        //act
        blackjack.drawCards(sam, dealer, 4);
        int actual = sam.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldHaveCorrectTotalForDealerWhenDealerWins() {
        //arrange
        int expected = 18;
        Player sam = new Player("sam");
        Player dealer = new Player("dealer");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingDealerAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(sam, dealer, deckOfCards);
        blackjack.addCardsToDeckForTestingDealerAsWinner();
        //act
        blackjack.drawCards(sam, dealer, 4);
        int actual = dealer.getTotal();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void drawCards_validPlayersAndIndex_shouldReturnDealer() {
        //arrange
        Player sam = new Player("sam");
        Player expected = new Player("expected");
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.addCardsToDeckForTestingDealerAsWinner();
        Blackjack blackjack = new Blackjack();
        blackjack.handOutCards(sam, expected, deckOfCards);
        blackjack.addCardsToDeckForTestingDealerAsWinner();
        //act
        Player actual = blackjack.drawCards(sam, expected, 4);
        //assert
        assertEquals(expected, actual);
    }
}