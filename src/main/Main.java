package main;

import blackjack.Blackjack;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Blackjack blackjack = new Blackjack();
        blackjack.play();
    }
}