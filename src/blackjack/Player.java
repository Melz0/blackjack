package blackjack;

import java.util.ArrayList;
import java.util.Objects;

public class Player {
    private final String name;
    private int total;
    private ArrayList<String> cards;

    protected Player(String name) {
        this.name = name;
    }

    protected String getName() {
        return name;
    }

    protected int getTotal() {
        return total;
    }

    protected ArrayList<String> getCards() {
        return cards;
    }

    /**
     * Adds input list of cards to the players list of cards.
     * @param cards A list of cards
     */
    protected void setCards(ArrayList<String> cards) {
        this.cards = cards;

        int rank;
        int sum = 0;

        for (String c : cards) {
            if (c.substring(1).equals("A")) {
                rank = 11;
            }
            else if (c.substring(1).equals("J") || c.substring(1).equals("Q")
                    || c.substring(1).equals("K")) {
                rank = 10;
            }
            else {
                rank = Integer.parseInt(c.substring(1));
            }
            sum += rank;
        }

        total = sum;
    }

    /**
     * Adds input card to players list of cards.
     * @param card
     */
    protected void addCard(String card) {
        cards.add(card);

        if (card.substring(1).equals("A")) {
            total += 11;
        }
        else if (card.substring(1).equals("J") || card.substring(1).equals("Q")
                || card.substring(1).equals("K")) {
            total += 10;
        }
        else {
            total += Integer.parseInt(card.substring(1));
        }
    }

    /**
     * @return Checks if player has blackjack.
     */
    protected boolean blackJack() {
        boolean hasAce = false;
        boolean hasCardOfValue10 = false;

        for (String c : cards) {
            if (c.contains("A")) {
                hasAce = true;
            }
            if (c.contains("10") || c.contains("J") || c.contains("Q") || c.contains("K")) {
                hasCardOfValue10 = true;
            }
        }
        return hasAce && hasCardOfValue10;
    }

    /**
     * @return Checks if player has 22 points.
     */
    protected boolean has22() {
        return cards.get(0).contains("A") && cards.get(1).contains("A");
    }

    protected StringBuilder showCards() {
        StringBuilder show = new StringBuilder(name + ": ");

        for (int i = 0; i < cards.size() - 1; i++) {
            show.append(cards.get(i)).append(", ");
        }

        show.append(cards.get(cards.size() - 1));

        return show;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return total == player.total && name.equals(player.name) && cards.equals(player.cards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, total, cards);
    }
}