package blackjack;

import java.util.ArrayList;

public class Blackjack {
    DeckOfCards deck;

    /**
     * Adds list of cards from input DeckOfCards to deck.
     * Hands of first and third card in deck to player sam, and second and fourth card to the dealer.
     * @param player
     * @param dealer
     * @param deckOfCards
     */
    protected void handOutCards(Player player, Player dealer, DeckOfCards deckOfCards) {
        deck = deckOfCards;

        ArrayList<String> playerCards = new ArrayList<>();
        ArrayList<String> dealerCards = new ArrayList<>();

        playerCards.add(deck.getDeck().get(0));
        dealerCards.add(deck.getDeck().get(1));
        playerCards.add(deck.getDeck().get(2));
        dealerCards.add(deck.getDeck().get(3));

        player.setCards(playerCards);
        dealer.setCards(dealerCards);
    }

    /**
     * Contains the logic for when neither sam nor the dealer has blackjack or 22 points.
     * @param player
     * @param dealer
     * @param index
     * @return
     */
    protected Player drawCards(Player player, Player dealer, int index) {
        if (player.getTotal() < 17) {
            if (index < deck.getDeck().size()) {
                player.addCard(deck.getDeck().get(index));
            }
            if (index < deck.getDeck().size() - 1) {
                drawCards(player, dealer, index + 1);
            }
            if (player.getTotal() > 21) {
                return dealer;
            }
        }
        else {
            if (dealer.getTotal() <= player.getTotal()) {
                if (index < deck.getDeck().size()) {
                    dealer.addCard((deck.getDeck().get(index)));
                }
                if (index < deck.getDeck().size() - 1) {
                    drawCards(player, dealer, index + 1);
                }
                if (dealer.getTotal() > 21) {
                    return player;
                }
            }
        }

        if (dealer.getTotal() > player.getTotal() && dealer.getTotal() <= 21) {
            return dealer;
        }
        else {
            return player;
        }
    }

    /**
     * Decides the winner of the game.
     * @param player
     * @param dealer
     * @return
     */
    private Player winner(Player player, Player dealer) {
        if (player.blackJack()) {
            return player;
        }
        else if (player.has22()) {
            return dealer;
        }
        else {
            return drawCards(player, dealer, 4);
        }
    }

    /**
     * Returns the String that should be shown when the game is over.
     * @param player
     * @param dealer
     * @param deck
     * @return
     */
    private String toString(Player player, Player dealer, DeckOfCards deck) {
        handOutCards(player, dealer, deck);
        Player winner = winner(player, dealer);
        return winner.getName() + "\n" + player.showCards() + "\n" + dealer.showCards();
    }

    /**
     * When the game is started the text in this method is printet out to standard out.
     */
    protected void greeting() {
        System.out.println("""
                Hello there ;)\s
                Write the URI of a file containing a shuffled deck of cards.\s
                The file should be in the following format: `CA, D4, H7, SJ,..., S5, S9, D10`\s
                If you don't have a file, click enter and I'll make a shuffled deck for you :)\s
                Have fun, darling <3""");
    }

    /**
     * Starts the game.
     */
    public void play() {
        greeting();

        Player sam = new Player("sam");
        Player dealer = new Player("dealer");

        DeckOfCards deck = new DeckOfCards();
        deck.addCardsToDeck();

        System.out.println(toString(sam, dealer, deck));
    }

    protected void addCardsToDeckForTestingSamAsWinner() {
        deck = new DeckOfCards();
        deck.addCardsToDeckForTestingSamAsWinner();
    }

    protected void addCardsToDeckForTestingDealerAsWinner() {
        deck = new DeckOfCards();
        deck.addCardsToDeckForTestingDealerAsWinner();
    }
}