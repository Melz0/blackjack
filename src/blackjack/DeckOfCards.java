package blackjack;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Scanner;

public class DeckOfCards {
    private ArrayList<String> deck;

    protected ArrayList<String> getDeck() {
        return deck;
    }

    /**
     * Gets every word, separated by comma and space, from input file written in command line.
     * Then adds the words to deck.
     * If no file is provided, a shuffled list of cards is made and added to deck.
     */
    protected void addCardsToDeck() {
        Scanner sc = new Scanner(System.in);
        String userinput = sc.nextLine();

        if (!(Objects.equals(userinput, ""))) {
            try {
                File file = new File(userinput);
                Scanner scan = new Scanner(file);
                scan.useDelimiter(", ");
                //Adding the tokenized Strings to deck
                deck = new ArrayList<>();
                while(scan.hasNext()){
                    deck.add(scan.next());
                }
                //Closing the scanner
                scan.close();
            }
            catch(Exception e) {
                System.out.println("Oopsie! Seems like something went wrong. Check if the file path is valid.");
            }
        }
        else {
            addCardsToDeckWithoutFile();
            Collections.shuffle(deck);
        }
    }

    /**
     * Makes a list of cards.
     */
    protected void addCardsToDeckWithoutFile() {
        deck = new ArrayList<>();

        String[] suits = {"C", "D", "H", "S"};
        String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        for (String s : suits) {
            for (String r : ranks) {
                String card = s+r;
                deck.add(card);
            }
        }
    }

    protected void addCardsToDeckForTestingHandOutCards() {
        deck = new ArrayList<>();
        deck.add("HA");
        deck.add("D2");
        deck.add("S3");
        deck.add("S4");
    }

    protected void addCardsToDeckForTestingSamAsWinner() {
        deck = new ArrayList<>();
        deck.add("D0");
        deck.add("D0");
        deck.add("D0");
        deck.add("D0");
        deck.add("HA");
        deck.add("D7");
        deck.add("SA");
        deck.add("S7");
        deck.add("SA");
    }

    protected void addCardsToDeckForTestingDealerAsWinner() {
        deck = new ArrayList<>();
        deck.add("D0");
        deck.add("D0");
        deck.add("D0");
        deck.add("D0");
        deck.add("HA");
        deck.add("D6");
        deck.add("SA");
        deck.add("S7");
    }
}



