# Blackjack


## The game model:
* A single deck of playing cards
* Two players (called Sam and the Dealer) who will play against each other
* Each player is given two cards from the top of a shuffled deck of cards.
* Cards are given in the following order: [sam, dealer, sam, dealer].

## Rules:
* Numbered cards are their point value while Jack, Queen and King count as 10 and Ace
counts as 11.
* If either player has Blackjack with their initial hand and they win the game.
Blackjack is an initial score of 21 with two cards: A + [10, J, Q, K].
* Sam wins when both players starts with Blackjack.
* Dealer wins when both players starts with 22 (A + A).
* If neither player has Blackjack, then Sam can start drawing cards from the top
of the deck.
* Sam must stop drawing cards from the deck if their total reaches 17 or higher.
* Sam has lost the game if their total is higher than 21.
* When Sam has stopped drawing cards the dealer can start drawing cards from
the top of the deck.
* The dealer must stop drawing cards when their total is higher than sam's.
* The dealer has lost the game if their total is higher than 21.

## Input:
The game is able to read a file containing a deck of cards, taking the reference to the file as a command line argument, as a starting point.
If no file is provided, a new shuffled deck of 52 unique cards will be initialised.
The list is in the following format: `CA, D4, H7, SJ,..., S5, S9, D10`.

## Testing
The solution includes unit tests.


## Maintained by

[Melissa Lila Taktaz Tuncer](https://gitlab.com/Melz0)
